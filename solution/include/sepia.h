#ifndef SEPIA_H
#define SEPIA_H

#include "image.h"

void pixel_sepia_asm(struct pixel* src_data, struct pixel* dst_data);

enum image_create sepia_c(const struct image* source, struct image* new_img);

enum image_create sepia_asm(const struct image* source, struct image* new_img);

#endif // SEPIA_H
