#ifndef UTIL_H
#define UTIL_H

#include <stdint.h>
#include <stdio.h>

enum open_status {
    OPEN_OK,
    OPEN_FAILED
};

enum open_status open_file(FILE** file, const char* path, const char* modes);
int close_file(FILE* file);

#endif  // UTIL_H
