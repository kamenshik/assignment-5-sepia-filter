#ifndef ROTATE_H
#define ROTATE_H

#include <stdint.h>
#include <stdlib.h>

#include "image.h"

enum image_create rotate_image(const struct image* source, struct image* new_img);

#endif // ROTATE_H
