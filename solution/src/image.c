#include "../include/image.h"

enum image_create create_image(const uint64_t width, const uint64_t height, struct image* img) {
    if (img == NULL) {return CREATE_FAILED;}
    img->height = height;
    img->width = width;
    img->data = malloc(sizeof(struct pixel) * width * height * 3);
    if (img->data == NULL) {return CREATE_FAILED;}
    return CREATE_OK;
}

void free_image(struct image* img) {
    if (!img || !img->data) {
        return;
    }
    free(img->data);
    img->data = NULL;
}
struct pixel* image_find_pixel(const uint64_t x, const uint64_t y, const struct image* img) {
    return img->data + x + y * img->width;
}
