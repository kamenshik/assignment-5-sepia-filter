#include <time.h>
#include <stdio.h>
#include "../include/bmp.h"
#include "../include/sepia.h"
#include "../include/util.h"

static int read_file_error_handler(enum read_status read_file_result) {
    switch (read_file_result) {
        case READ_OK:
            break;
        case CREATE_IMAGE_ERROR:
            fprintf(stderr, "Allocate Error");
            return 1;
        case READ_INVALID_SIGNATURE:
            fprintf(stderr, "Signature corrupted");
            return 1;
        case READ_INVALID_HEADER:
            fprintf(stderr, "Header corrupted");
            return 1;
        case READ_INVALID_BITS:
            fprintf(stderr, "Body corrupted");
            return 1;
        default:
            fprintf(stderr, "Unexpected Error");
            return 1;
    }
    return 0;
}

static int handling_closing_file(FILE* file) {
    if (close_file(file) != 0) {
        fprintf(stderr, "Can't close file");
        return 1;
    }
    return 0;
}

static int test(
        const struct image source,
        const char* output_filepath,
        const char* string_result_output,
        enum image_create sepia_func(const struct image*, struct image*)) {
    FILE* out = NULL;
    struct image new_img = {0};
    clock_t start, end;
    start = clock();
    const enum image_create state = sepia_func(&source, &new_img);
    end = clock();
    if (state == CREATE_FAILED) {
        fprintf(stderr, "Allocate Error");
        return 1;
    }
    if (open_file(&out, output_filepath, "wb") == OPEN_FAILED) {
        fprintf(stderr, "Can't write in file");
        return 1;
    }
    const enum write_status write_result = to_bmp(out, &new_img);
    free_image(&new_img);
    if (handling_closing_file(out) != 0) {
        return 1;
    }
    if (write_result == WRITE_ERROR) {
        printf("Create bmp error");
        return 1;
    }
    printf(string_result_output, ((double) (end - start)) / CLOCKS_PER_SEC * 1000);
    return 0;
}

int main(int argc, char** argv) {
    if (argc != 4) {
        fprintf(stderr, "Wrong number of arguments %d instead of 4", argc);
        return 1;
    }

    const char* input_filepath = argv[1];
    const char* output_filepath_c = argv[2];
    const char* output_filepath_asm = argv[3];

    FILE* in = NULL;
    if (open_file(&in, input_filepath, "rb") == OPEN_FAILED) {
        fprintf(stderr, "Can't read from file");
        return 1;
    }
    struct image source = {0};
    const enum read_status read_file_result = from_bmp(in,&source);
    if (handling_closing_file(in) != 0) {
        return 1;
    }
    const int result_read_file_exception_handler = read_file_error_handler(read_file_result);
    if (result_read_file_exception_handler != 0) {
        return result_read_file_exception_handler;
    }

    const int result_test_c = test(
            source,
            output_filepath_c,
            "[C] time of execution: %f milliseconds\n",
            sepia_c
    );
    if (result_test_c != 0) {
        return result_test_c;
    }
    const int result_test_asm = test(
            source,
            output_filepath_asm,
            "[ASM] time of execution: %f milliseconds\n",
            sepia_asm
    );
    if (result_test_asm != 0) {
        return result_test_asm;
    }
    free_image(&source);
    return 0;
}
