#include "../include/image.h"
#include "../include/sepia.h"

static inline uint8_t color_value_alignment(uint16_t val) {
return val <= 255 ? val : 255;
}

static inline struct pixel pixel_sepia(struct pixel* pixel) {
    return (struct pixel) {
            .r = color_value_alignment(
                    (uint16_t) (0.393 * pixel->r + 0.769 * pixel->g + 0.189 * pixel->b)
            ),
            .g = color_value_alignment(
                    (uint16_t) (0.349 * pixel->r + 0.686 * pixel->g + 0.168 * pixel->b)
            ),
            .b = color_value_alignment(
                    (uint16_t) (0.272 * pixel->r + 0.534 * pixel->g + 0.131 * pixel->b)
            )
    };
}

enum image_create sepia_c(const struct image* source, struct image* new_img) {
    const enum image_create state = create_image(source->width, source->height, new_img);
    for (uint32_t y=0; y < source->height; y++) {
        for (uint32_t x=0; x < source->width; x++) {
            *image_find_pixel(x, y, new_img) = pixel_sepia(image_find_pixel(x, y, source));
        }
    }
    return state;
}

enum image_create sepia_asm(const struct image* source, struct image* new_img) {
    const enum image_create state = create_image(source->width, source->height, new_img);
    const uint32_t image_size = source->height * source->width;
    for (uint32_t i = 0; i < image_size; i += 4) {
        pixel_sepia_asm(source->data + i, new_img->data + i);
    }
    return state;
}
