#include "../include/rotate.h"

enum image_create rotate_image(const struct image* source, struct image* new_img) {
    const enum image_create state = create_image(source->height, source->width, new_img);
    for (uint32_t y=0; y < source->height; y++) {
        for (uint32_t x=0; x < source->width; x++) {
            *image_find_pixel(source->height - 1 - y, x, new_img) = *image_find_pixel(x, y, source);
        }
    }
    return state;
}
