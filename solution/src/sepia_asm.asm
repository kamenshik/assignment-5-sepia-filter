; To process the first, second and third packets we need different combinations of bytes in the xmm registers.
; In first packet we need to do 3 operations for first pixel and 1 operation for second pixel,
; so we will need 3 copies first pixel bytes and 1 copy second pixel bytes.
; It looks something like this:
; For first pack:
; xmm0:  b2     b1     b1     b1
; xmm1:  g2     g1     g1     g1
; xmm2:  r2     r1     r1     r1
; ==================================
; calc:  new_b2 new_r1 new_g1 new_b1

; For second pack:
; xmm0:  b3     b3     b2     b2
; xmm1:  g3     g3     g2     g2
; xmm2:  r3     r3     r2     r2
; ==================================
; calc:  new_g3 new_b3 new_r2 new_g2

; For third pack:
; xmm0:  b4     b4     b4     b3
; xmm1:  g4     g4     g4     g3
; xmm2:  r4     r4     r4     r3
; ==================================
; calc:  new_r4 new_g4 new_b4 new_r3
%macro shuffle_data 1  ; %1 - packs's number (1, 2, 3)
    ; Select shuffle pattern
    %ifnum %1
        %if %1 == 1    ; first pack
        %define shuffle_pattern 0b11000000

        %elif %1 == 2  ; second pack
        %define shuffle_pattern 0b11110000

        %elif %1 == 3  ; third pack
        %define shuffle_pattern 0b11111100

        %else
            %error "Supported only numbers 1, 2, 3"
        %endif
    %else
        %error "Expected number"
    %endif
    ; shuffle
    shufps xmm0, xmm0, shuffle_pattern
    shufps xmm1, xmm1, shuffle_pattern
    shufps xmm2, xmm2, shuffle_pattern
%endmacro

; Macro with all operations needed for handling pack
%macro pack_handling 1  ; %1 - pack number
    ;;; Clearing xmm0, xmm1 and xmm2 ;;;
    pxor xmm0, xmm0
    pxor xmm1, xmm1
    pxor xmm2, xmm2

    ;;; Load data ;;;
    ; put pixels in xmm-registers
    ; xmm-registers are 16 bytes long
    ; each pixel contains 3 bytes uint-numbers
    ; Pixel will be 12 bytes, because after converting to floats each number becomes 4 bytes.

    ; first pack:  new_b1, new_g1, new_r1, new_b2
    ; second pack: new_g2, new_r2, new_b3, new_g3
    ; third pack:  new_r3, new_b3, new_g3, new_r3
    pinsrb xmm0, [rdi], 0           ; b[i] -> xmm0[0]
    pinsrb xmm1, [rdi + 1], 0       ; g[i] -> xmm1[0]
    pinsrb xmm2, [rdi + 2], 0       ; r[i] -> xmm2[0]

    pinsrb xmm0, [rdi + 3], 12      ; b[i + 1] -> xmm0[12]
    pinsrb xmm1, [rdi + 3 + 1], 12  ; g[i + 1] -> xmm1[12]
    pinsrb xmm2, [rdi + 3 + 2], 12  ; r[i + 1] -> xmm2[12]

    shuffle_data %1

    ;;; Apply sepia filter ;;;
    cvtdq2ps xmm0, xmm0
    cvtdq2ps xmm1, xmm1
    cvtdq2ps xmm2, xmm2
    ; multiply by sepia-matrix
    mulps xmm0, xmm3
    mulps xmm1, xmm4
    mulps xmm2, xmm5
    ; calculate sum
    addps xmm0, xmm1
    addps xmm0, xmm2
    ; convert to ints
    cvtps2dq xmm0, xmm0
    ; align the values
    mov r8, max_matrix_values
    pminsd xmm0, [r8]

    ;;; Writing results ;;;
    pextrb [rsi], xmm0, 0
    pextrb [rsi + 1], xmm0, 4
    pextrb [rsi + 2], xmm0, 8
    pextrb [rsi + 3], xmm0, 12
%endmacro

; prepare before next pack handling
%macro next_pack 0
    ; moving data pointers before going for next batch
    add rdi, 3  ; go to next pixel
    add rsi, 4  ; move pointer over 4 values which we used in calculations in previous packet
    ; shuffle sepia matrix
    shufps xmm3, xmm3, shuffle_pattern_for_sepia
    shufps xmm4, xmm4, shuffle_pattern_for_sepia
    shufps xmm5, xmm5, shuffle_pattern_for_sepia
%endmacro


%define shuffle_pattern_for_sepia 0b01111001

section .rodata
align 16
initial_matrix_for_blue: dd 0.131, 0.168, 0.189, 0.131   ; multipliers for blue
align 16
initial_matrix_for_green: dd 0.534, 0.686, 0.769, 0.534  ; multipliers for green
align 16
initial_matrix_for_red: dd 0.272, 0.349, 0.393, 0.272    ; multipliers for red
align 16
max_matrix_values: dd 0xFF, 0xFF, 0xFF, 0xFF             ; max values

section .text
global pixel_sepia_asm
pixel_sepia_asm:
    mov r8, initial_matrix_for_blue
    movdqa xmm3, [r8]
    mov r8, initial_matrix_for_green
    movdqa xmm4, [r8]
    mov r8, initial_matrix_for_red
    movdqa xmm5, [r8]
    pack_handling 1
    next_pack
    pack_handling 2
    next_pack
    pack_handling 3
    ret
