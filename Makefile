DIR_ROOT = solution
DIR_SRC = $(DIR_ROOT)/src
DIR_INCLUDE = $(DIR_ROOT)/include
DIR_TARGET = $(DIR_ROOT)/bin
SOURCES := $(shell find $(DIR_SRC) -name '*.c')
INCLUDES := $(shell find $(DIR_INCLUDE) -name '*.h')

CCOMP = gcc
CFLAGS = -O2
ASM = nasm
ASMFLAGS = -felf64

$(DIR_TARGET)/sepia: $(DIR_TARGET)/sepia_asm.o $(INCLUDES) $(SOURCES)
	$(CCOMP) $(CFLAGS) -I $(DIR_INCLUDE) -o $@  $(SOURCES) $<


$(DIR_TARGET)/sepia_asm.o: $(DIR_SRC)/sepia_asm.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

clean:
	rm -rm out

